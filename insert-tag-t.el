;;; insert-tag-t.el --- insert HTML tag <a></a> for jpg file -*- coding: utf-8 -*-
;;
;; Copyright (C) 2019 Taisuke NISHIMORI
;;
;; Author: Taisuke Nishimori <taisuke.nishimori@nifty.com>
;;
;; Version: 0.0.2
;; Keywords: comm
;; License: GPL-2.0+
;; Web site: http://t-nishimori.ddo.jp/diary/
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.
;;
;;; Commentary:
;;
(defvar insert-tag-t-year "2019")
(defvar insert-tag-t-work-dir "~/tmp")
;
(defun insert-tag-t ()
  (interactive)
  (let ((list (directory-files insert-tag-t-work-dir nil (concat "^" (insert-tag-t-read-date nil) ".+L\.jpg$")))
	large-name
	small-name
	)
    (insert "<p>\n")
    (while list
      (setq large-name (car list))
      (setq small-name (concat (car (split-string large-name "\\(L\\.jpg\\)")) ".jpg"))
      (insert (concat "<a href=\"../imgs/" insert-tag-t-year "/"))
      (insert large-name)
      (insert (concat "\" rel=\"lightbox\"><img src=\"../imgs/" insert-tag-t-year "/"))
      (insert small-name)
      (insert "\" border=\"0\"></a>\n<br>\n")
      (setq list (cdr list))
      ) ;while
    (insert "</p>\n")
    ) ;let
  ) ;defun

(defun insert-tag-t-read-date (date)
  (let ((start-of-2digits-year 2))
  (while (not (string-match
	       "[0-9][0-9][0-9][0-9][0-9][0-9]"
	       (setq date (read-string "Date: "
				       (if (boundp 'tdiary-diary-the-day)
					   (substring tdiary-diary-the-day start-of-2digits-year) ;yyyymmdd --> yymmdd
					 (format-time-string "%y%m%d" (current-time)))))))
    nil);while
  );let
  date);defun
(provide 'insert-tag-t)
