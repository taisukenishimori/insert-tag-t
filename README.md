# README #

### これは？ ###

* emacs lispコード。
* バッファに画像ファイル名の挿入とHTMLタグを追加するためのツール
* 0.0.2

### どうやって使うのか ###

* emacsでロードして使う。具体的には以下の通り。
* insert-tag-t.elをemacsのsite-lispディレクトリに保存する
* 例えば、init.elで(require 'insert-tag-t)と追記する
* ファイル名が以下の画像ファイルを特定のディレクトリに用意する。
    * yymmdd-hhmmssL.jpg (大きいサイズのファイル)
    * yymmdd-hhmmss.jpg (HTMLに表示されるファイル)
* 特定のディレクトリは、例えば"~/tmp"など(デフォルト)。
* 拡張子は"JPG"ではなく、"jpg"を前提としている。insert-tag-t.elの中身を書き換えれば、拡張子はなんでもよい。
* emacs 26.2で作成、動作確認した。

